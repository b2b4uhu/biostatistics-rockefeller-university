# README #

To clone this repository, type at command line: 

```
$ git clone https://biostatsRU@bitbucket.org/biostatsRU/biostatistics-rockefeller-university
```




### What is this repository for? ###

* This repository is intended to make it available a series of functions developed at the Biostatistics Core at the Rockefeller University to perform the most common tasks in statistical analysis and to provide examples of datasets that can be used in training and educational activities.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* You can contact the repository owner by writing to joelrosa@uol.com.br